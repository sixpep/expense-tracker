var express = require('express')
var router = express.Router()
const User = require('../models/user.model')
const auth = require('../middlewares/auth')
const https = require('https')
const jwt = require('jsonwebtoken')

/** 
 * send otp
 * @url
 * /users/send-otp 
 * @method
 * post
 * @request
 * @body @params
 * mobile_number [string][Mandatory][Ex:916305602096]
 * @response
 * @body
 * 200 - 'otp sent.'
 * 400 - 'missing parameters.' or any other error messages
 **/
router.post('/send-otp', async function (req, res, next) {
  if (!req.body.mobile_number)
    res.status(400).send('missing parameters.')

  var url = `https://api.msg91.com/api/v5/otp?authkey=${process.env.MSG91_AUTH_KEY}&template_id=${process.env.MSG91_TEMPLATE_ID}&mobile=${req.body.mobile_number}`
  https.get(url, (response) => {
    response.on('data', data => {
      try {
        var jsondata = JSON.parse(data)
        if (jsondata.type === 'error')
          res.status(400).send(jsondata.message)
        else
          res.status(200).send('otp sent.')
      }
      catch (error) {
        next(error)
      }
    })
  }).on('error', (error) => {
    next(error)
  })
})

/** 
 * verify otp
 * @url
 * /users/verify-otp
 * @method
 * post 
 * @request
 * @body @params
 * mobile_number  [string][Mandatory]
 * otp            [number][Mandatory]
 * @response
 * @header
 * authorization   [string]
 * @body
 * 200 - 'otp verified'
 * 400 - 'missing parameters' or any other error messages
 **/
router.post('/verify-otp', async function (req, res, next) {
  if (!req.body.otp || !req.body.mobile_number)
    res.status(400).send('missing parameters.')

  var url = `https://api.msg91.com/api/v5/otp/verify?mobile=${req.body.mobile_number}&otp=${req.body.otp}&authkey=${process.env.MSG91_AUTH_KEY}`
  https.get(url, (response) => {
    response.on('data', data => {
      try {
        var jsondata = JSON.parse(data)
        if (jsondata.type === 'error')
          res.status(400).send(jsondata.message)
        else {
          const token = jwt.sign({ mobile_number: req.body.mobile_number }, process.env.SECRET_KEY)
          res.status(200).header('authorization', token).send('otp verified.')
        }
      }
      catch (error) {
        next(error)
      }
    })
  }).on('error', (error) => {
    next(error)
  })
})

/** 
 * create account
 * @url
 * /users/
 * @method
 * post
 * @request
 * @header
 * authorization  [string]
 * @body @params
 * name           [string][Mandatory]
 * gender         [string][Optional]['MALE','FEMALE'][Default: 'MALE']
 * dob            [number][Optional]
 * @response
 * @Headers
 * authorization   [string] [new token]
 * @body
 * 200 - {
    "gender": "MALE",
    "_id": "5ea04b0818b7e6720d8bc1d4",
    "name": "vamshi",
    "mobile_number": "916305602096",
    "expenses": [],
    "__v": 0
}
 * 400 - any error messages
 * 401 - 'access denied. no token provided.'
 * 403 - 'invalid token.'
 **/
router.post('/', auth, async function (req, res, next) {
  req.body.mobile_number = req.decoded.mobile_number
  try {
    var user = new User(req.body)
    await user.save()
    const token = user.generateAuthToken()
    res.status(200).header('authorization', token).send(user)
  }
  catch (error) {
    next(error)
  }
})

/** 
 * update account
 * @url
 * /users/
 * @method
 * put
 * @request
 * @header
 * authorization  [string]
 * @body @params
 * name           [string][Optional]
 * gender         [string][Optional]['MALE','FEMALE'][Default: 'MALE']
 * dob            [number][Optional]
 * @response
 * @body
 * 200 - 'update success.'
 * 400 - 'update failed.' or any error messages
 * 401 - 'access denied. no token provided.'
 * 403 - 'invalid token.'
 **/
router.put('/', auth, async function (req, res, next) {
  if (req.body.mobile_number)
    delete req.body.mobile_number
  console.log(req.body)

  try {
    var user = await User.findByIdAndUpdate(req.decoded._id, req.body, { runValidators: true, new: true })
    if (user)
      res.status(200).send('update success.')
    else
      res.status(400).send('update failed.')
  }
  catch (error) {
    next(error)
  }
})

/** 
 * get account
 * @url
 * /users/
 * @method
 * get
 * @request
 * @header
 * authorization  [string]
 * @response
 * @body
 * 200 - {
    "gender": "MALE",
    "_id": "5ea04b0818b7e6720d8bc1d4",
    "name": "vamshi",
    "mobile_number": "916305602096",
    "expenses": [],
    "__v": 0
}
 * 400 - any error messages
 * 401 - 'access denied. no token provided.'
 * 403 - 'invalid token.'
 **/
router.get('/', auth, async function (req, res, next) {
  try {
    var user = await User.findById(req.decoded._id)
    res.status(200).send(user)
  }
  catch (error) {
    next(error)
  }
})

/** 
 * add new expense
 * @url
 * /users/expenses
 * @method
 * post
 * @request
 * @header
 * authorization    [string]
 * @body @params
 * amount           [string][Mandatory]
 * category         [string][Mandatory]
 * day              [number][Mandatory]
 * month            [number][Mandatory]
 * @response
 * @body
 * 200 - 'add expense success.'
 * 400 - any error messages
 * 401 - 'access denied. no token provided.'
 * 403 - 'invalid token.'
 **/
router.post('/expenses', auth, async function (req, res, next) {
  try {
    var user = await User.findByIdAndUpdate(req.decoded._id, { $push: { expenses: req.body } }, { runValidators: true, new: true })
    if (user)
      res.status(200).send('add expense success.')
    else
      res.status(400).send('add expense failed.')
  }
  catch (error) {
    next(error)
  }
})

module.exports = router
