const jwt = require('jsonwebtoken')

module.exports = function (req, res, next) {
    //get the token from the header if present
    const token = req.headers['authorization']
    //if no token found, return response (without going to the next middelware)
    if (!token) {
        return res
            .status(401)
            .send('access denied. no token provided.')
    }

    try {
        //if can verify the token, set req.user and pass to next middleware
        req.decoded = jwt.verify(token, process.env.SECRET_KEY)
        next()
    } catch (ex) {
        //if invalid token
        res.status(403).send('invalid token.')
    }
}
