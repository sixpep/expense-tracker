const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')

let userSchema = new mongoose.Schema({
    name: { type: String, required: true },
    gender: { type: String, enum: ['MALE', 'FEMALE'], default: 'MALE' },
    dob: { type: Number },
    mobile_number: { type: String, required: true },
    expenses: [{
        amount: { type: Number, min: 0, required: true },
        category: { type: String, required: true },
        day: { type: Number, max: 6, required: true, default: 0 },
        month: { type: Number, max: 11, required: true, default: 0 }
    }]
})

//custom method to generate authToken
userSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id }, process.env.SECRET_KEY)
    return token
}

module.exports = mongoose.model('User', userSchema)